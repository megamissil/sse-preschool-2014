<?php
	include('postman/_variables2.php');
	include( $_SERVER['DOCUMENT_ROOT'].'/tyfoon/connect.php' );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
	$aFeatured = pageByCategory('ANNOUNCEMENTS', 'ANY', 0 , 3 , 'PUBL_ASC');
?>

<?php
	include("header.php");
?>

<div id="homepage_main">
	<div class="slider">
		<div><img src="img/slider-1.jpg" alt="slider 1"></div>
		<div><img src="img/slider-3.jpg" alt="slider 3"></div>
		<div><img src="img/slider-4.jpg" alt="slider 4"></div>
		<div><img src="img/slider-5.jpg" alt="slider 5"></div>
		<div><img src="img/slider-6.jpg" alt="slider 6"></div>
		<div><img src="img/slider-2.jpg" alt="slider 2"></div>
	</div>
	<div id="info-bar">
		<div class="row">
			<div class="large-5 medium-5 hide-for-small columns">
				<div class="announcements">
					<h2>Announcements</h2>
						<?php foreach( $aFeatured as $aArticle) {?>
	            			<li>
	            				<a href="<?php echo $aArticle['url']; ?>"><?php echo $aArticle['title']; ?></a><br />
				                <?php echo ''.date('m/d/Y', strtotime( $aArticle['published'] )).$aArticle['msg_short'].''; ?>
	             			</li>
	          			<?php } ?>
				</div>
			</div>
			<div class="large-4 medium-5 hide-for-small columns">
				<div class="register">
					<h2>Register Now!</h2>
					<p>Enter your email address below to receive more information about registration, and more.</p>
					<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
						<input type="email" name="email" id="email" value="<?=$_POST['email'] ?>" placeholder="your email address"><input type="submit" name="sbmtbtn" id="sbmtbtn" value="Submit">
					</form>
				</div>
			</div>
			<div class="large-3 medium-2 hide-for-small columns">
				<div class="newsletter">
					<a href="pdf/February_2014.pdf"><img src="img/icon_newsletter.png" alt="" /><br />
					Download our latest Newsletter</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="info-bar">
	<div class="row">
		<div class="show-for-small columns">
			<div class="announcements">
				<h2>Announcements</h2>
					<?php foreach( $aFeatured as $aArticle) {?>
            			<li>
            				<a href="<?php echo $aArticle['url']; ?>"><?php echo $aArticle['title']; ?></a><br />
			                <?php echo ''.date('m/d/Y', strtotime( $aArticle['published'] )).$aArticle['msg_short'].''; ?>
             			</li>
          			<?php } ?>
			</div>
		</div>
		<div class="show-for-small small-8 columns">
			<div class="register">
				<h2>Register Now!</h2>
				<p>Enter your email address below to receive more information about registration, and more.</p>
				<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
					<input type="email" name="email" id="email" value="<?=$_POST['email'] ?>" placeholder="your email address"><input type="submit" name="sbmtbtn" id="sbmtbtn" value="Submit">
				</form>
			</div>
		</div>
		<div class="show-for-small small-4 columns">
			<div class="newsletter">
				<a href="pdf/February_2014.pdf"><img src="img/icon_newsletter.png" alt="" /><br />
				Download our latest Newsletter</a>
			</div>
		</div>
	</div>
</div>

<?php
	include("footer.php");
?>