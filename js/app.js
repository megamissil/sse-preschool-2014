// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){
	$('.multiple-items').slick({
	  infinite: true,
	  slidesToShow: 6,
	  slidesToScroll: 6
	});

	$('.slider').slick({
	  fade: true,
	  autoplay: true
	});
});