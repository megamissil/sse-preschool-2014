<?php
include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet( 77 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'About';
	$cSEOTitle = '';
	$layout = 'subpage';
?>

<?php
	include("header.php");
?>

<div id="sub_main">
	<div class="row">
		<div class="large-9 medium-8 columns">
			<h1><?php echo $aPage['title']; ?></h1>
				<?php echo $aPage['msg']; ?>
		</div>
		<div class="large-3 medium-4 columns">
			<div class="contact-box">
				<h2>Contact Information</h2>
				<p><strong>Director:</strong> Leslie Dunlap<br />
				<strong>Email:</strong> leslie@ssechurch.org</p>
				
				<p><strong>Assistant Director:</strong> Mary Ann Sherman<br />
				<strong>Tigger Time Director:</strong> Jennifer Bemowski</p>
	
				<p><strong>Phone:</strong> 205-967-6317</p>
				
				<p>Saint Stephen's Preschool<br />
				3775 Crosshaven Drive<br />
				Birmingham, AL 35223</p>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>