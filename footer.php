<footer>
	<div id="footer" class="hide-for-medium-down">
	 	<div class="navf">
	        <ul>
	          <li><a href="#">About Us</a>
	          	<ul class="footer-links">
	          		<li><a href="about.php">About Us</a></li>
        			<li><a href="mothers-day-out.php ">Mother’s Day Out</a></li>
        			<li><a href="preschool.php">Preschool</a></li>
       				<li><a href="5k.php">5K</a></li>
        			<li><a href="parent-handbook.php">Parent Handbook</a></li>
        			<li><a href="curriculum.php">Curriculum</a></li>
      			</ul>
      		 </li>
	          <li><a href="#">Registration</a>
	          	<ul class="footer-links">
        			<li><a href="fall-registration.php">Fall</a></li>
        			<li><a href="summer-registration.php">Summer</a></li>
        			<li><a href="current-openings.php">Current Openings</a></li>
      			</ul>
      		 </li>
	          <li><a href="#">Extended Care</a>
	          	<ul class="footer-links">
	          		<li><a href="early-birds.php">Early Birds</a></li>
       				<li><a href="tigger-time.php">Tigger Time</a></li>
	          	</ul>
	          </li>
	          <li><a href="#">What's Happening</a>
	          	<ul class="footer-links">
	          		<li><a href="newsletter.php">Newsletter</a></li>
        			<li><a href="pdf/2014-15_Calendar.pdf" target="_blank">School Calendar</a></li>
	          	</ul>
	          </li>
	          <li><a href="contact-us.php">Contact Us</a></li>
	        </ul>
		    </div>
	    </div>
	</div>
	<div class="footer-bar">
		<div class="row">
			<div class="large-12 medium-4 small-4 columns">
				<p class="copyright">Copyright Saint Stephen's Preschool <?=date('Y') ?></p>
			</div>
			<div class="medium-6 small-4 columns show-for-medium-down">
				<div class="social">
		            <ul class="small-block-grid-3">
		                <li><a href="https://www.facebook.com/StStephensPreschool" target="_blank"><img src="img/icon_fb_ft.png" alt="" /></a></li>
		                <li><a href="https://twitter.com/sspreschool" target="_blank"><img src="img/icon_tw_ft.png" alt="" /></a></li>
		                <li><a href="mailto:leslie@ssechurch.org"><img src="img/icon_em_ft.png" alt="" /></a></li>
		            </ul>
        		</div>
			</div>
			<div class="medium-2 small-4 columns">
				<a href="https://www.zeekeeinteractive.com/" target="_blank"><img src="img/zeekee-slug-white-2011.png" class="zeekee" alt="zeekee interactive" /></a>
			</div>
		</div>
	</div>
</footer>

<script src="bower_components/jquery/jquery.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="js/foundation/foundation.offcanvas.js"></script>
<script src="jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>				
<script src="js/app.js"></script>
</body>
</html>