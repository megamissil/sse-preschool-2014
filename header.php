<!doctype html>
<html class="no-js" lang="en">
  <!-- <![endif] -->
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width" />
    <meta content='<?php echo $cMetaDesc; ?>' name='description'>
    <meta content='<?php echo $cMetaKW; ?>' name='keywords'>
    <title><?php echo $cPageTitle; ?> - St. Stephen's Preschool</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
    <link rel="stylesheet" type="text/css" href="stylesheets/slick.css"/>
    <script type="text/javascript" src="//use.typekit.net/egs5tpt.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <script src="bower_components/modernizr/modernizr.js"></script>
    	<!-- IE Fix for HTML5 Tags -->
    	<!--[if lt IE 9]>
      	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    	<![endif]-->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXX-Y']);
			_gaq.push(['_trackPageview']);
			
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
    </script>
  </head>
  <body class="<?=$layout ?>">
<header>
  <div class="row">
    <div class="large-5 columns">
      <div class="row logo">
        <div class="large-4 medium-6 hide-for-medium columns">
          <a href="index.php"><img src="img/new-logo-small.png" alt="" /></a>
        </div>
        <div class="large-4 show-for-medium columns">
          <a href="index.php"><img src="img/new-logo-med.png" alt="" /></a>
        </div>
        <div class="large-8 columns">
          <a href="index.php"><h1>Saint Stephen's <span id="header-color">Preschool</span></a><br />
          <small>Birmingham, Alabama</small></h1>
        </div>
      </div>
    </div>
    <div class="large-2 large-offset-5 show-for-large-up columns">
        <div class="social">
            <ul class="small-block-grid-3">
                <li><a href="https://www.facebook.com/StStephensPreschool" target="_blank"><img src="img/icon_fb.png" alt="" /></a></li>
                <li><a href="https://twitter.com/sspreschool" target="_blank"><img src="img/icon_tw.png" alt="" /></a></li>
                <li><a href="mailto:leslie@ssechurch.org"><img src="img/icon_em.png" alt="" /></a></li>
            </ul>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name"></li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>
        <section class="top-bar-section">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li class="has-dropdown"><a href="#">About Us &#x25BC;</a>
              <ul class="dropdown">
                <li><a href="about.php">About Us</a></li>
                <li><a href="mothers-day-out.php">Mother’s Day Out</a></li>
                <li><a href="preschool.php">Preschool</a></li>
                <li><a href="5k.php">5K</a></li>
                <li><a href="parent-handbook.php">Parent Handbook</a></li>
                <li><a href="curriculum.php">Curriculum</a></li>
              </ul>
            </li>
            <li class="has-dropdown"><a href="#">Registration &#x25BC;</a>
              <ul class="dropdown">
                <li><a href="fall-registration.php">Fall</a></li>
                <li><a href="summer-registration.php">Summer</a></li>
                <li><a href="current-openings.php">Current Openings</a></li>
              </ul>
            </li>
            <li class="has-dropdown"><a href="#">Extended Care &#x25BC;</a>
              <ul class="dropdown">
                <li><a href="early-birds.php">Early Birds</a></li>
                <li><a href="tigger-time.php">Tigger Time</a></li>
              </ul>
            </li>
            <li class="has-dropdown"><a href="#">What's Happening &#x25BC;</a>
              <ul class="dropdown">
                <li><a href="newsletter.php">Newsletter</a></li>
                <li><a href="pdf/2014-15_Calendar.pdf" target="_blank">School Calendar</a></li>
                <li><a href="contact-us.php">Contact Us</a></li>
              </ul>
            </li>
          </ul>
        </section>
       </nav>
    </div>
  </div>

<div class="divider"></div>
</header>