<?php
	include('postman/_variables.php');
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet( 76 );
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Contact';
	$cSEOTitle = '';
	$layout = 'subpage';
?>

<?php
	include("header.php");
?>
<div id="sub_main">
	<div class="row">
		<div class="large-6 medium-5 columns">
			<h1><?php echo $aPage['title']; ?></h1>
				<?php echo $aPage['msg']; ?>
		</div>
		<div class="large-6 medium-7 columns">
				<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
					<div class="row">
						<div class="large-8 medium-6 columns">
							<label>Name:</label>
								<input type="text" name="name" value="<?=$_POST['name'] ?>"/>
						</div>
					</div>
						
					<div class="row">
						<div class="large-8 medium-6 columns">
							<label>Phone Number:</label>
								<input type="text" name="phone" value="<?=$_POST['phone'] ?>"/>
						</div>
					</div>

					<div class="row">
						<div class="large-8 medium-6 columns">
							<label>Email:</label>
								<input type="text" name="email" value="<?=$_POST['email'] ?>"/>
						</div>
					</div>

					<div class="row">
						<div class="large-8 medium-6 columns">
							<label>Message:</label>
								<textarea id="comments" value="<?=$_POST['msg'] ?>" name="comments"></textarea>
						</div>
					</div>
					<input type="submit" class="button" value="Submit" name="sbmtbtn" id="sbmtbtn">
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	include("footer.php");
?>